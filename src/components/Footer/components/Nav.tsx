import React from 'react'
import styled from 'styled-components'
import { contractAddresses } from '../../../farm/lib/constants'
import { getEthChainInfo } from '../../../utils/getEthChainInfo'
import githubLogo from '../../../assets/img/github.png'
import { GITHUB } from '../../../constants/config'

const { ethscanType, chainId } = getEthChainInfo()

const contractAddressesTemp = contractAddresses as { [index: string]: any }

const Nav: React.FC = () => {
  return (
    <StyledNav>

    <StyledLink
      target="_blank"
      href={`https://www.koingfu.com`}
    >
    APR Chart
    </StyledLink>

      <StyledLink
        target="_blank"
        href={`https://smartscan.cash/token/${contractAddressesTemp.erc20[chainId]}#code`}
      >
        SEP20 Contract
      </StyledLink>

      <StyledLink
        target="_blank"
        href={`https://smartscan.cash/address/0x537efEC636f0A7E73d8d035aC986160af4979377`}
      >
        Farming Contract
      </StyledLink>
    </StyledNav>
  )
}

const StyledNav = styled.nav`
  align-items: center;
  display: flex;
`

const StyledLink = styled.a`
  color: #8cd4e4;
  padding-left: ${(props) => props.theme.spacing[3]}px;
  padding-right: ${(props) => props.theme.spacing[3]}px;
  text-decoration: none;
  &:hover {
    color: #8cd4e4;
    text-decoration: underline;
  }
  img {
    height: 19px;
  }
`

export default Nav
