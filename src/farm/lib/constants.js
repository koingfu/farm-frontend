// 42 - Testnet
// 4 - Rinkeby
// 1 - Mainnet

/*
1 Mainnet
3 Ropsten
4 Rinkeby
5 Goerli
42 Kovan
56 Binance Smart Chain Mainnet
97 Binance Smart Chain Testnet
100 xDai
137 Matic
1287 Moonbase Testnet
80001 Matic Testnet
43113 Avalanche Testnet
43114 Avalanche Mainnet
*/

export const contractAddresses = {
  erc20: {
    10000: '0x4524cE998c2551CdA6d5763E0AD74153059B6207',

  },
  erc20v2: {
    10000: '0x4524cE998c2551CdA6d5763E0AD74153059B6207',

  },
  farm: {
    10000: '0x537efEC636f0A7E73d8d035aC986160af4979377',

  },
  weth: {
    10000: '0xc4eb62f900ae917f8F86366B4C1727eb526D1275',

  },
}

export const supportedPools = [
  {
    id: 'Ki',
    version: '',
    name: 'Ki LP Farming',
    pid: 0,
    lpAddresses: {
      10000: '0x5401FcdD5172A062de628302Cb360e6061A08209',
    },
    tokenAddresses: { ...contractAddresses.erc20 },
    symbol: 'Ki-FlexUSD LP',
    tokenSymbol: 'Ki',
    icon: '',
    pool: '100%',
  },
  
]
